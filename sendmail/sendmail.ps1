# Common PS smtp notification script with 2 parameters 
# V.2022Apr18
# By Yip

# Declare script parameters
param($a,$b)

# Declare Mail parameters
$smtp = New-Object System.Net.Mail.SmtpClient
$to = New-Object System.Net.Mail.MailAddress("receiver@rhino.auto")
$from = New-Object System.Net.Mail.MailAddress("Sender_Noreply@163.com")
$msg = New-Object System.Net.Mail.MailMessage($from, $to)

# Add other recipients if required
$msg.To.Add("cc@rhino.auto")

# Fill in mail content
$msg.subject = "Server $a is Lost"
$msg.body = "The server $a lost connection from $b, Please check!"

# Declare credential, mail server parameters & send
$smtp.host = "smtp.163.com"
$password = "smtppassword"
$secureString = $password | ConvertTo-SecureString -AsPlainText -Force
$smtpCred = New-Object System.Management.Automation.PSCredential -ArgumentList $From,$secureString
$smtp.Credentials = $smtpCred
$smtp.send($msg)
