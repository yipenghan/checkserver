# Common PS smtp notification script with 2 parameters 
# V.2022Apr22
# By Yip

# Claim the failed server array, incase notification action duplication happens
[System.Collections.ArrayList]$lostsvr=($null,$null)

# Start loop by check if the server list exists
while (Test-Path .\servers.txt){
$date = date

# Read server list and put into loop
foreach ($server in Get-Content .\servers.txt) 
{
# 1st round to test connection on items in server list one by one
if (!(test-connection $server -quiet))
    {
# wait 5 -Seconds and do one more test for the lost server from the 1st round check, and if it's not in the failed history array        
        start-sleep -Seconds 5
        if (!(test-connection $server -quiet) -and !($lostsvr.Contains($server)))
            {
            # prompt on host screen and call the send mail compoment by adding server name/ip and check time(near the 1st round check) 
            echo "Catch the lazy bone $server sleeping from $date !"
               .\sendmail.ps1 $server $date
            # add the confirmed failed one into the failed server array
             $lostsvr += $server

            }
      }
      else
      {
     # remove the server from the failed server array as soon as the server turn to pingable in the 1st round
      $lostsvr.Remove($server)
      }
} 
# wait for 20 seconds when all items in server list being checked, then start another checking
start-sleep -Seconds 20
                }
# write to screen and remind for check the server list file
Write-Host "Please check if servers.txt exists!"

# 
## <EOF>
